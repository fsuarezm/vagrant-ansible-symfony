# -*- mode: ruby -*-
# vi: set ft=ruby :

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"

parameters = File.expand_path("../Parameters", __FILE__)
load parameters

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
    config.vm.box = $base_box
    #config.vm.box_url = $base_box_url

    config.vm.network :private_network, ip: $private_network_ip
    config.vm.network :forwarded_port, host: 8000, guest: 80
    config.ssh.forward_agent = true
    config.vm.synced_folder "../", "/var/www/html", id: "vagrant-root", :nfs => true

    config.vm.provider :virtualbox do |v|
      v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
      v.customize ["modifyvm", :id, "--cpus", "2"]
      v.customize ["modifyvm", :id, "--memory", 1024]
      v.customize ["modifyvm", :id, "--name", $vm_name]
    end

    # Provisioning with Ansible
    config.vm.provision :ansible do |ansible|
      ansible.playbook = "playbook/main.yml"
      ansible.extra_vars = {
        vhost: $vm_name,
        database_name: $database_name,
        database_user: $database_user,
        database_password: $database_password,
        php_repository: $php_repository,
        php_timezone: $time_zone
      }
    end
end
