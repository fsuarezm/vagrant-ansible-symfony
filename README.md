# Vagrant-Ansible environment for Symfony2 

Provides a virtual environment for Symfony2 development usign Vagrant and Ansible.

## Prerequisites

  * VirtualBox
  * Vagrant
  * Ansible

## Install

After you have installed the prerequisites softwares, just clone this repository on the root of your Symfony project.

    $ cd tour_project
    $ git@bitbucket.org:fsuarezm/vagrant-ansible-symfony.git vagrant

Copy the ```Parameters.dist``` file into ```Parameters``` file

    $ cp vagrant/Parameters.dist vagrant/Parameters

Modify your Parameters file upon your wishes and then run vagrant:

    $ cd vagrant
    $ vagrant up --provision

## Configuration

This project is pre-configured to install an Ubuntu 14.04 eith PHP, MySQL, Apache2 and phpmyadmin. It also create a network
interface with the IP 10.10.10.10

Automatically setups database with this setup:

    * Username: symfony
    * Password: symfony1
    * Database: symfony

For default debug, change ```app.php```

        $env = getenv('APP_ENV') ? getenv('APP_ENV') : 'prod';
        $debug = (bool)getenv('APP_DEBUG');
        $kernel = new AppKernel($env, $debug);

Enjoy!
Feel free to contribute to this project ;)
